
FROM python

RUN apt -y update
RUN apt -y upgrade
RUN pip install --user pipenv
RUN pip3 install Django
RUN pip3 install "graphene-django>=2.0"



#RUN python3 -V
#RUN django-admin --version
